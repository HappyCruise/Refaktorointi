package kivipaperisakset;

/**
 *
 * @author Ira Dook
 */
public class Peli {

	/**
	 * Luo ja aloittaa pelin
	 * @param args 
	 */
	public static void main(String args[]) {
		//Luo peli
		PeliMoottori peli = new PeliMoottori();
		//Aloita peli
		peli.pelaa();
	}
}
