package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kivipaperisakset.PeliMoottori;

/**
 * 
 * @author Leevi
 * Testaa pelin funktioiden toimivuus
 */
public class PeliTest {

	PeliMoottori peli; 
	
	
	/**
	 * Luodaan uusi peli testeille
	 * 
	 */
	@BeforeEach
	public void init() {
		peli = new PeliMoottori();

	}
	
	
	
	/**
	 * Testaa voittajan valinta
	 * 
	 */
	@Test
	public void testVoittaja() {
		//Tasapeli
		assertEquals(peli.getWinner("kivi", "kivi"), 0);
		
		//P1 voittaa
		assertEquals(peli.getWinner("kivi", "sakset"), 1);
		
		//P2 voittaa
		assertEquals(peli.getWinner("kivi", "paperi"), 2);
	}
	
	
	
	/*
	 * Testaa pisteiden nostamista
	 * 
	 */
	@Test
	public void testPisteNosto() {
		//Pelaaja 1 voittaa
		assertEquals(peli.getP1Voitot(), 0);
		peli.raiseScore(1);
		assertEquals(peli.getP1Voitot(), 1);
		
		//Pelaaja 2 voittaa
		assertEquals(peli.getP2Voitot(), 0);
		peli.raiseScore(2);
		assertEquals(peli.getP2Voitot(), 1);
		
		
		//Tasapeli
		peli.raiseScore(0);
		assertEquals(peli.getP1Voitot(), 1);
		assertEquals(peli.getP2Voitot(), 1);
	}
	

	
	/**
	 * Testaa päättyykö peli
	 * 
	 */
	@Test
	public void testPeliLoppui() {
		//Pelaaja 1 voitti
		assertTrue(peli.checkGameOver(3, 2));
		
		//Pelaaja 2 voitti
		assertTrue(peli.checkGameOver(0, 3));
		
		//Kumpikaan pelaaja ei ole voittanut
		assertFalse(peli.checkGameOver(2, 1));
	}	
}
