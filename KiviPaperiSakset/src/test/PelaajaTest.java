package test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kivipaperisakset.Pelaaja;

/**
 * 
 * @author Leevi
 *
 *	Testaa Pelaaja oliota
 */
public class PelaajaTest {
	/**
	 * Testien Pelaaja
	 */
	Pelaaja pelaaja;
	

	/**
	 * 
	 * Luodaan uusi pelaaja jokaiselle testille
	 */
	@BeforeEach
	public void init() {
		pelaaja = new Pelaaja();
	}

	
	/**
	 * 
	 * Testaa pelaajan voittojen nosto
	 */
	@Test
	public void testVoitot() {
		//Uuden pelaajan voitot 0
		assertEquals(pelaaja.getVoitot(), 0);
		
		//Nostetaan yhdellä
		pelaaja.raiseVoitot();
		
		//Voitot 1
		assertEquals(pelaaja.getVoitot(), 1);
	}
	
	
	/**
	 * 
	 * Testaa pelaajan valinta
	 */
	@Test
	public void testValinta() {
		boolean sisaltaa;
		String[] valinnat =  new String[]{"kivi", "paperi", "sakset"};
		
		List<String> valintaList = new ArrayList<>(Arrays.asList(valinnat));
		String valinta;
		
		
		//Kokeillaan useaan otteeseen, satunnaisuuden vuoksi
		valinta = pelaaja.pelaajanValinta();
		assertTrue(valintaList.contains(valinta));
		valinta = pelaaja.pelaajanValinta();
		assertTrue(valintaList.contains(valinta));
		valinta = pelaaja.pelaajanValinta();
		assertTrue(valintaList.contains(valinta));
	}
	

}
