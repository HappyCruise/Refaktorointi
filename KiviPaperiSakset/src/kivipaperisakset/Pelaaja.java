
package kivipaperisakset;

/**
 *
 * @author Ira Dook
 * Pelaaja kivi sakset paperi peliin.
 * 
 */
public class Pelaaja {

	/**
	 * Voittojen lukumäärä
	 * 
	 */
    int voitot = 0;      

    /**
     * Valitse randomilla kivi, paperi tai sakset
     * 
     * @return valinta
     */
    public String pelaajanValinta() {
        String valinta = "";
        //Arvo valinta
        int c = (int) (Math.random() * 3);
        
        //Valitse string valinta arvon mukaan
        switch (c) {
            case 0:
                valinta = ("kivi");
                break;
            case 1:
                valinta = ("paperi");
                break;
            case 2:
                valinta = ("sakset");
                break;
        }
        return valinta;
    }

    /**
     * Nostaa pelaajan voittoja yhdellä
     * 
     */
    public void raiseVoitot() {
        voitot++;
    }

    /**
     * Pelaajan tämänhetkiset voitot.
     * 
     * @return voitot
     */
    public int getVoitot() {
        return (voitot);
    }
}
