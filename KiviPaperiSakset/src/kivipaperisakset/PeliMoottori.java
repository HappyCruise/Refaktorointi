package kivipaperisakset;

/**
 * 
 * @author Leevi
 * Pelimoottori kivi sakset paperi pelille
 *
 */
public class PeliMoottori {

	//Pelin tilanne
	private int tasapelit;
	private int pelatutPelit;
	
	
	//Pelaajat
	private Pelaaja p1;
	private Pelaaja p2;
	private String p1Valinta;
	private String p2Valinta;
	
	//Onko peli päättynyt
	private boolean peliLoppui;

	/**
	 * Luo pelimoottori
	 */
	public PeliMoottori() {
		this.p1 = new Pelaaja();
		this.p2 = new Pelaaja();
		peliLoppui = false;
	}

	/**
	 * Pelaajan 1 voitot
	 * @return voitot
	 */
	public int getP1Voitot() {
		return p1.getVoitot();
	}

	/**
	 * Pelaajan 2 voitot
	 * @return voitot
	 */
	public int getP2Voitot() {
		return p2.getVoitot();
	}

	/**
	 * Pelaa peli
	 */
	public void pelaa() {
		int kierroksenVoittaja; //viimeisimmän kierroksen voittaja

		do {
			// Pelaajien valinnat
			p1Valinta = p1.pelaajanValinta();
			p2Valinta = p2.pelaajanValinta();

			// Kierroksen tiedot
			printKierrosInfo();

			// Valitaan voittaja
			kierroksenVoittaja = getWinner(p1Valinta, p2Valinta);

			// Nostetaaan pisteitä
			raiseScore(kierroksenVoittaja);
			peliLoppui = checkGameOver(getP1Voitot(), getP2Voitot());
			
			pelatutPelit++;
			System.out.println();
			
		} while (!peliLoppui); // Jatka kierroksia kunnes toinen pelaajista voittaa

	}
	

	/**
	 * Tarkista päättyikö peli
	 * @param voitot1 pelaajan 1 voitot
	 * @param voitot2 pelaajan 2 voitot
	 * @return päättyikö peli
	 */
	public boolean checkGameOver(int voitot1, int voitot2) {
		boolean peliOhi = false;
		
		// Tarkistetaanko onko toinen pelaajista voittanut
		if (voitot1 >= 3 || voitot2 >= 3) {
			peliOhi = true;
			System.out.println("KOLME VOITTOA - PELI PÄÄTTYY");
		}
		
		return peliOhi;
	}

	/**
	 * Tulosta kierroksen erä, tasapelit ja pelaajien voitot.
	 */
	public void printKierrosInfo() {
		System.out.println("Erä: " + pelatutPelit + " =====================\n");
		System.out.println("Tasapelien lukumäärä: " + tasapelit + "\n");

		System.out.println("Pelaaja 1: " + p1Valinta + "\n\t Pelaaja 1:llä koossa " + p1.getVoitot() + " voittoa.");
		System.out.println("Pelaaja 2: " + p2Valinta + "\n\t Pelaaja 2:lla koossa " + p2.getVoitot() + " voittoa.");
	}

	/**
	 * 
	 * Nosta voittajan pisteitä tai tasapelejä
	 * @param kierroksenVoittaja 0 = tasapeli, 1/2 = Pelaaja 1/2
	 */
	public void raiseScore(int kierroksenVoittaja) {
		//Tasapeli
		if (kierroksenVoittaja == 0) {
			tasapelit++;
			System.out.println("\n\t\t\t Tasapeli \n");
		} 
		
		//Pelaaja 1 voitti
		else if (kierroksenVoittaja == 1) {
			System.out.println("Pelaaja 1 voittaa");
			p1.raiseVoitot();
		} 
		
		//Pelaaja 2 voitti
		else if (kierroksenVoittaja == 2) {
			System.out.println("Pelaaja 2 voittaa");
			p2.raiseVoitot();
		}
	}

	/**
	 * Valitse kierroksen voittaja
	 * @param p1Valinta pelaajan 1 valinta
	 * @param p2Valinta pelaajan 2 valinta
	 * @return voittaja, 0 = tasapeli, 1/2 = voittanut pelaaja
	 */
	public int getWinner(String p1Valinta, String p2Valinta) {
		
		//Valitse voittaja valintojen mukaan ja palauta voittajan numero
		if ((p1Valinta.equals("kivi")) && (p2Valinta.equals("paperi"))) {
			return 2;
		} else if ((p1Valinta.equals("kivi")) && (p2Valinta.equals("sakset"))) {
			return 1;
		} else if ((p1Valinta.equals("paperi")) && (p2Valinta.equals("kivi"))) {
			return 1;
		} else if ((p1Valinta.equals("paperi")) && (p2Valinta.equals("sakset"))) {
			return 2;
		} else if ((p1Valinta.equals("sakset")) && (p2Valinta.equals("kivi"))) {
			return 2;
		} else if ((p1Valinta.equals("sakset")) && (p2Valinta.equals("paperi"))) {
			return 1;
		}
		
		//Tasapeli
		return 0;
	}

}
